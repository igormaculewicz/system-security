package pl.regon;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws IOException {

        if (args.length < 3) {
            System.out.println("java -jar Main.jar <(encrypt|decrypt)> <file> <password>");
            return;
        }

        Path inputFile = Paths.get(args[1]);
        String text = new String(Files.readAllBytes(inputFile));
        String fileName = inputFile.getFileName().toString().replaceFirst("[.][^.]+$", "");

        switch (args[0]) {
            case "encrypt":
                String encrypted = encrypt(text, args[2]);

                Files.write(Paths.get(fileName + ".enc"), encrypted.getBytes());
                System.out.println(String.format("Encrypted file: %s", inputFile.getFileName()));
                break;
            case "decrypt":
                String decrypted = decrypt(text, args[2]);

                Files.write(Paths.get(fileName + ".dec"), decrypted.getBytes());
                System.out.println(String.format("Decrypted file: %s", inputFile.getFileName()));
                break;
            default:
                throw new RuntimeException("Unknown mode!");
        }
    }

    /**
     * Encrypts given payload with given password.
     *
     * @param payload  given payload.
     * @param password given password
     * @return return encrypted payload with given password.
     */
    public static String encrypt(String payload, String password) {

        //Perform cryptographic operation.
        List<Long> cryptographicResult = cryptographicMethod(payload.toCharArray(), password.toCharArray());

        //Formats every long to hexadecimal number padded to 4 bytes, and concatenate it.
        return cryptographicResult.stream()
                .map(number -> String.format("%1$04x", number))
                .collect(Collectors.joining());
    }

    /**
     * Decrypts given encrypted payload with given password.
     *
     * @param encryptedPayload given encrypted payload.
     * @param password         given password.
     * @return returns encrypted payload if password was correct.
     */
    public static String decrypt(String encryptedPayload, String password) {
        //Reversing password(It is necessary to perform decryption[Xoring in reversed order])
        password = new StringBuilder(password).reverse().toString();

        //Divide string to 4 byte octets, and convert them to char array.
        char[] encryptedPayloadCharArray = new String(longListToCharArray(Arrays.stream(splitStringToOctets(encryptedPayload, 4))
                .map(octet -> {
                    try {
                        return Integer.valueOf(octet, 16).longValue();
                    } catch (NumberFormatException e) {
                        throw new NumberFormatException(String.format("Cannot convert octet '%s' to integer!", octet));
                    }
                }).collect(Collectors.toList()))).toCharArray();


        //Performs cryptographic method on encrypted char array payload and reversed password. Then convert long list to string.
        return new String(longListToCharArray(cryptographicMethod(encryptedPayloadCharArray, password.toCharArray())));

    }

    /**
     * Method that XOR every payload array position, by all password positions.
     *
     * @param payload  given payload array.
     * @param password given password array.
     * @return return list of longs which is a result of XOR.
     */
    private static List<Long> cryptographicMethod(char[] payload, char[] password) {
        List<Long> result = new ArrayList<>();

        //For every letter of payload
        for (char letter : payload) {
            long encryptResult = letter;

            //And every letter of password
            for (char passwordLetter : password) {
                //Perform XOR operation
                encryptResult = encryptResult ^ passwordLetter;
            }

            result.add(encryptResult);
        }

        return result;
    }

    private static char[] longListToCharArray(List<Long> longs) {
        return longs.stream()
                .map(octet -> (char) octet.longValue())
                .map(Object::toString)
                .collect(Collectors.joining()).toCharArray();
    }

    private static String[] splitStringToOctets(String text, int octetSize) {
        List<String> parts = new ArrayList<>();

        int length = text.length();
        for (int i = 0; i < length; i += octetSize) {
            parts.add(text.substring(i, Math.min(length, i + octetSize)));
        }
        return parts.toArray(new String[0]);
    }
}