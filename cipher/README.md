# File encrypt and decrypt

### Requirements
You have to had Java 8+ installed.

### How to run?
Simply execute **run.sh**. It would compile Java class, and then run an example.


### How encryption and decryption algorithm works?
- Encryption algorithm takes a every character of data and perform XOR operation with every character of password.
Then saves every character as 4 bytes hexadecimal octet.
- Decryption algorithm splitting encrypted data to 4 bytes hexadecimal octets, convert them to characters and then
takes a every character of data and perform XOR operation with every character of reversed password.

### Algorithm abstraction
#### Encryption
```text
payload = Test
password = abc

encrypted = (((T^a)^b)^c) + (((e^a)^b)^c) + (((s^a)^b)^c) + (((t^a)^b)^c) = 0034 0005 0013 0014

```

#### Decryption
```text
encrypted = 0034 0005 0013 0014
password = abc

decrypted = (((34^c)^b)^a) + (((5^c)^b)^a) + (((13^c)^b)^a) + (((14^c)^b)^a) = Test
```

### Example usage
#### Encryption
We have plain text file with name **test_file.txt**:
```text
Ala ma kota a kot ma alę!
```

And we encrypting it with command:
```sh
java -jar Main.jar encrypt test_file.txt 12345678
```
result of this command should be encrypted file with the same name and *.enc extension.
```text
004900640069002800650069002800630067007c006900280069002800630067007c00280065006900280069006401110029
```

#### Decryption
We have encrypted file with name **test_file.enc**:
```text
004900640069002800650069002800630067007c006900280069002800630067007c00280065006900280069006401110029
```

And we decrypting it with command:
```sh
java -jar Main.jar decrypt test_file.enc 12345678
```
result of this command should be decrypted file with the same name and *.dec extension.

```text
Ala ma kota a kot ma alę!
```

#### Example output of run.sh
```text
EXECUTION
Text to encrypt: Ala ma kota a kot ma alę!
Encrypted file: test_file.txt
Encrypted text: 004900640069002800650069002800630067007c006900280069002800630067007c00280065006900280069006401110029
Decrypted file: test_file.enc
Decrypted text: Ala ma kota a kot ma alę!
```

### References
[Source code](https://bitbucket.org/igormaculewicz/system-security/src/master/cipher/)

