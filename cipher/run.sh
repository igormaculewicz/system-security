#!/bin/bash

#Compiling
echo "COMPILATION"
rm -rf build 2> /dev/null
mkdir build
javac -d ./build *.java
cd build
jar cvfe Main.jar pl.regon.Main *
echo ""

#Execution
echo ""
echo "EXECUTION"
payload="Ala ma kota a kot ma alę!"
echo "Text to encrypt: $payload"
echo -e "$payload\c" >> test_file.txt
java -jar Main.jar encrypt test_file.txt 12345678
echo "Encrypted text: `cat test_file.enc`"
java -jar Main.jar decrypt test_file.enc 12345678
echo "Decrypted text: `cat test_file.dec`"
