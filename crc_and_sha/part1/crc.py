import os
import re

import crcmod.predefined


def only_digits(string: str):
    return re.sub("\D", "", string)


def get_filename_without_extension(filename: str):
    return os.path.splitext(os.path.basename(filename))[0]


def calculate_string_crc(string: str, crc_func=crcmod.predefined.mkCrcFun('crc-32')):
    return crc_func(string.encode())


def generate_crc_file(filename, chunk_size):
    if chunk_size.isdecimal():
        chunk_size = int(chunk_size)
    else:
        print("Chunk size must be an decimal!")
        return

    try:
        with open(filename) as input_file:
            with open("{}.crc".format(get_filename_without_extension(filename)), "w+") as result_file:
                result_file.write("{}\n".format(chunk_size))
                index = 1
                while True:
                    chunk = input_file.read(chunk_size)
                    if not chunk:
                        break

                    crc = calculate_string_crc(chunk)
                    result_file.write("{}\n".format(crc))
                    print("Chunk({}) {}".format(index, crc))
                    index += 1
    except FileNotFoundError as e:
        print(e)


def check_file_crc(filename: str, crc_filename: str):
    try:
        with open(crc_filename) as crc_file:
            chunk_size = int(only_digits(crc_file.readline()))

            if not chunk_size:
                print("File must start by chunk_size!")
                return

            with open(filename) as file:
                index = 1

                while True:
                    chunk = file.read(chunk_size)
                    chunk_crc = calculate_string_crc(chunk)
                    control_crc = only_digits(crc_file.readline())

                    if bool(chunk) ^ bool(control_crc):
                        print("File failed integrity check, because there is difference in amount of chunks and "
                              "checksums!")
                        return

                    elif not chunk and not control_crc:
                        print("File '{}' is integral with '{}'".format(filename, crc_filename))
                        return

                    if chunk_crc == int(control_crc):
                        print("Chunk({})({}) is integral!".format(index, chunk_crc))
                        index += 1
                    else:
                        print("Chunk({}) have violated [file: {}, crc_file: {}] integrity!".format(index, chunk_crc,
                                                                                                   control_crc))
                        break

    except FileNotFoundError as e:
        print(e)


def handle_generate_mode():
    filename = input("Enter a file name: ")
    chunk_size = input("Enter a chunk size: ")
    generate_crc_file(filename, chunk_size)
    return


def handle_check_mode():
    filename = input("Enter a file name: ")
    crc_file_name = input("Enter a CRC file name: ")
    check_file_crc(filename, crc_file_name)
    return


while True:
    mode = input("Enter mode(g=generate, c=check, e=exit): ")

    if mode == "g":
        handle_generate_mode()
    elif mode == "c":
        handle_check_mode()
    elif mode == "e":
        exit(0)
    else:
        print("Unknown mode!")
