from crcmod import crcmod


def create_table():
    a = []
    for i in range(256):  # Generujemy 256 bajtową tablicę optymalizacyjną
        k = i  # Inicjalną wartością wejściową dla generowania danej pozycji jest i.
        for j in range(8):  # iterujemy po 8 bitach
            if k & 1:  # Jeżeli k jest nieparzyste
                k ^= 0x1db710640  # XORujemy je przez ustaloną dla wariantu CRC-32 wartość wielomianową.
            k >>= 1  # K dzielimy na 2 bez reszty.
        a.append(k)  # Zapisujemy wynik operacji do tablicy
    return a  # Zwracamy tablicę


def crc_update(buf, crc):
    crc ^= 0xffffffff  # XORujemy przez (2^32)-1
    for k in buf:  # iterujemy po każdym znaku bufora

        """
        Przesuwamy crc o 8 bitów w prawo(Czyli dzielimy bez reszty przez 2 do potęgi 8[(crc >> 8) == (crc//(2**8))])
        Następnie XORujemy to z wartością z tabeli o indexie wyliczonym poprzez XOR wartości ze zmiennej
        k z wynikiem iloczynu bitowego wartości ze zmiennej crc z liczbą 255.
        """
        crc = (crc >> 8) ^ crc_table[(crc & 0xff) ^ k]

    return crc ^ 0xffffffff  # XORujemy przez (2^32)-1


text_to_check = "The quick brown fox jumps over the lazy dog"

# own implementation crc-32.
crc_table = create_table()
own_crc = crc_update(text_to_check.encode(), 0)

# crcmod crc-32 variant
crc_func = crcmod.predefined.mkCrcFun('crc-32')
crcmod_crc = crc_func(text_to_check.encode())

if own_crc == crcmod_crc:
    print("Own crc-32 variant({}) is equals to crcmod crc-32 variant({})".format(own_crc, crcmod_crc))
else:
    print("Own crc and crcmod crc are not equal!")
