import os

import ijson

from crc_and_sha.part2.sha_utils import ShaUtils


class FlatFile:
    ACTIVE = "active"
    EXEMPT = "exempt"
    NONE = "none"

    def __init__(self, flat_file_name):
        if not os.path.isfile(flat_file_name):
            raise FileNotFoundError("Cannot open file in path: {}".format(flat_file_name))
        self.flat_file_name = flat_file_name

    def check_nip_and_nrb(self, nip, nrb):
        date = None
        rounds = None
        to_check = None

        with open(self.flat_file_name, 'rb') as input_file:
            parser = ijson.parse(input_file)
            for prefix, event, value in parser:
                if prefix == "naglowek.dataGenerowaniaDanych":
                    date = value
                if prefix == "naglowek.liczbaTransformacji":
                    rounds = value
                if prefix == "naglowek" and event == "end_map":
                    if not date:
                        raise SyntaxError("Nie udało się odczytać daty z pliku płaskiego! Sprawdź jego strukturę.")
                    if not rounds:
                        raise SyntaxError(
                            "Nie udało się odczytać ilości rund z pliku płaskiego! Sprawdź jego strukturę.")

                    to_check = ShaUtils.generate_sha512_rounds("{}{}{}".format(date, nip, nrb), rounds)
                if prefix == "skrotyPodatnikowCzynnych.item":
                    if value == to_check:
                        return self.ACTIVE
                if prefix == "skrotyPodatnikowZwolnionych.item":
                    if value == to_check:
                        return self.EXEMPT
            else:
                return self.NONE
