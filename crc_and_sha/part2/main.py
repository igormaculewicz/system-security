import time

from crc_and_sha.part2.flat_file import FlatFile

DEFAULT_FILE_PATH = "resources/20191121.json"
DEFAULT_NIP = "1120000411"
DEFAULT_NRB = "33144010558221640315760086"

ACTIVE = "Podatnik identyfikujący się numerem NIP '{}' z przypisanym rachunkiem '{}' jest podatnikiem czynnym."
EXEMPT = "Podatnik identyfikujący się numerem NIP '{}' z przypisanym rachunkiem '{}' jest podatnikiem zwolnionym."
NONE = "Podatnik identyfikujący się numerem NIP '{}' z przypisanym rachunkiem '{}' nie znajduje się na żadnej z list."

flat_file_path = input("Podaj ścieżkę do pliku płaskiego: ") or DEFAULT_FILE_PATH
nip = input("Podaj nip: ") or DEFAULT_NIP
nrb = input("Podaj numer rachunku bankowego: ") or DEFAULT_NRB

start = time.time()

flat_file = FlatFile(flat_file_path)
result = flat_file.check_nip_and_nrb(nip, nrb)

if result == FlatFile.ACTIVE:
    print_result = ACTIVE
elif result == FlatFile.EXEMPT:
    print_result = EXEMPT
else:
    print_result = NONE

print(print_result.format(nip, nrb))

end = time.time()
print("Czas wykonania: {} sekund(y).".format(end - start))
