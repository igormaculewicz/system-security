import hashlib


class ShaUtils:
    @staticmethod
    def generate_sha512_rounds(payload, rounds):
        for _ in range(int(rounds)):
            payload = hashlib.sha512(payload.encode()).hexdigest()
        return payload
